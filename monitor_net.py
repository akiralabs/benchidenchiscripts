#!/usr/bin/python3
from datetime import datetime
import sys, os, os.path, socket
from time import sleep

def test_net():
    hosts = ( 'www.google.com', 'www.fer.hr', 'www.kernel.org', 'www.carnet.hr' )

    failures = 0
    for host in hosts:
        try: 
            socket.create_connection((host, 80))
        except:
            failures += 1
            print("%s failed." % host)

    return failures

INTERVAL = 60
THRESHOLD1 = 3
THRESHOLD2 = 6
AP_INTERVALS = 10

def now():
    return datetime.now().isoformat()

def main():
    STATE = 1 # 1 = STA, 2 = AP
    flist = []
    while True:
        if STATE == 1:
            f = test_net()
            if f > 1:
                flist.append(f)
                print("%s: %d failures" % (now(), len(flist)))
                if len(flist) >= THRESHOLD2:
                    # start AP
                    print("%s: Starting AP." % (now(),))
                    flist = []
                    os.system("/opt/scripts/ap/runap.py &")
                    STATE = 2
                elif len(flist) >= THRESHOLD1:
                    # try restarting wifi
                    print("%s: Restarting wifi." % (now(),))
                    #os.system("ifdown wlan0")
                    #sleep(5)
                    #os.system("ifup wlan0")
                    #sleep(5)
            else:
                if len(flist) > 2:
                    print("%s: Recovery." % (now(),))
                flist = []
        elif STATE == 2:
            flist.append(0)
            if len(flist) >= AP_INTERVALS:
                print("%s: Retrying WiFi." % (now(),))
                os.system('killall hostapd')
                os.system('killall dnsmasq')
                sleep(1)
                os.system('ip addr flush dev wlan0')
                os.system('ifdown wlan0')
                sleep(1)
                os.system('ifup wlan0')
                flist = []
                STATE = 1
        sleep(INTERVAL)

if __name__ == '__main__':
    main()
