#!/usr/bin/python3
import sys,os,os.path,socket
from zlib import crc32

hostname = SSID = socket.gethostname()
wifipassword = str(crc32(hostname.encode('latin1') + b'DenchI'))
dir = os.path.dirname(__file__)

hostapd_conf = open("%s/templates/hostapd.conf" % dir).read()
hostapd_conf = hostapd_conf.replace('{{SSID}}', SSID).replace('{{wifipassword}}', wifipassword)
open('/tmp/hostapd.conf', 'w').write(hostapd_conf)

dnsmasq_conf = open("%s/templates/dnsmasq.conf" % dir).read()
dnsmasq_conf = dnsmasq_conf.replace('{{hostname}}', hostname)
open('/tmp/dnsmasq.conf', 'w').write(dnsmasq_conf)

print("WiFi password:", wifipassword)

os.system('killall dnsmasq')
os.system('ifdown wlan0')
os.system('ifconfig wlan0 up')
os.system('ifconfig wlan0 192.168.38.1/24')
os.system('dnsmasq -C /tmp/dnsmasq.conf')
os.system('%s/hostapd /tmp/hostapd.conf' % dir)


