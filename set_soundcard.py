#!/usr/bin/python3
import sys,os,os.path,re,subprocess
from configparser import ConfigParser

#  1 [Device         ]: USB-Audio - USB Audio Device
RE_USB = re.compile(r'^\s+(\d+).+USB-Audio.*$', re.M)


def main():
	m = RE_USB.findall(open('/proc/asound/cards').read())
	if not m:
		alsa_id = 0
	else:
		alsa_id = int(m[0])
	open('/etc/asound.conf', 'wt').write('defaults.pcm.card %d\ndefaults.ctl.card %d\n' % (alsa_id, alsa_id))
	print("Set ALSA default device to %d" % alsa_id)

	if os.path.exists('/etc/mopidy/mopidy.conf'):
		config = ConfigParser()
		config.read('/etc/mopidy/mopidy.conf')
		# output=alsasink device=hw:1,0
		config['audio']['output'] = 'alsasink device=hw:%d,0' % alsa_id
		config.write(open('/etc/mopidy/mopidy.conf', 'wt'))
		print("Set Mopidy device to hw:%d,0" % alsa_id)

if __name__ == '__main__':
	main()
